class SchoolMember:
    '''Любой человек в школе.'''
    def __init__(self, name, age):
        self.name = name
        self.age = age
        
    def __str__(self):
        return f"{self.name} is {self.age} years old"

    def description(self):
        '''Вывести описание.'''
        print('Name:"{0}" Age:"{1}"'.format(self.name, self.age), end=" ")
        
    def status(self, stat):
        '''Вывести роль'''
        print('Роль в школе:'.format(stat))

class Teacher(SchoolMember):
    '''Представляет преподавателя.'''
    def __init__(self, name, age, salary):
        SchoolMember.__init__(self, name, age)
        self.salary = salary

    def description(self):
        SchoolMember.description(self)
        print('Зарплата: "{0:d}"'.format(self.salary))
    
    def status(self,stat="Teacher"):
        print(stat)

class Student(SchoolMember):
    '''Представляет студента.'''
    def __init__(self, name, age, marks):
        SchoolMember.__init__(self, name, age)
        self.marks = marks

    def description(self):
        SchoolMember.description(self)
        print('Оценки: "{0:d}"'.format(self.marks))
    
    def status(self,stat="Student"):
        print(stat)
        

t = Teacher('Mrs. Ivanova', 37, 30000)
s = Student('Pushnoy', 22, 75)

members = [t, s]
for member in members:
    member.description() # работает как для преподавателя, так и для студента
    member.status()
    print()
